﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eSewaPayGateway
{
    public static class eSewaDB
    {
        public static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        public static SqlCommand cmd = new SqlCommand();
        public static SqlDataAdapter adap = new SqlDataAdapter();

        public static int InsertExceptionLog(string Module, string ClassName, string MethodName, string ErrorCode, string ExMessage, string ExStackTrace)
        {
            int temp = 0;

            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertExceptionLog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Module", Module);
                cmd.Parameters.AddWithValue("@ClassName", ClassName);
                cmd.Parameters.AddWithValue("@MethodName", MethodName);
                cmd.Parameters.AddWithValue("@ErrorCode", ErrorCode);
                cmd.Parameters.AddWithValue("@ExMessage", ExMessage);
                cmd.Parameters.AddWithValue("@ExStackTrace ", ExStackTrace);
                temp = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }

        public static DataTable GetPgCredential(string provider)
        {
            DataTable dt = new DataTable();

            try
            {
                con.Open();
                cmd = new SqlCommand("Sp_Get_PgCredentials", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Pvd", provider);
                SqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "GetPgCredential", "select", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return dt;
        }

        public static int InsertPaymentRequestDetails(string TrackId, string TId, string IBTrackId, string Name, string PaymentGateway, string Email, string Mobile, string Address, double TotalAmount, double OriginalAmount, string AgentId, string AgencyName, string Ip, string PgRequest, string ServiceType, string EncRequest, string Trip, double TotalPgCharges, double TransCharges, string ChargesType, string PostHtml)
        {
            int temp = 0;
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertPaymentDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TrackId", TrackId);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@PaymentGateway", PaymentGateway);
                cmd.Parameters.AddWithValue("@Email", Email);
                cmd.Parameters.AddWithValue("@Mobile", Mobile);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@Amount", TotalAmount);
                cmd.Parameters.AddWithValue("@OriginalAmount", OriginalAmount);
                cmd.Parameters.AddWithValue("@AgentId", AgentId);
                cmd.Parameters.AddWithValue("@AgencyName", AgencyName);
                cmd.Parameters.AddWithValue("@Status", "Requested");
                cmd.Parameters.AddWithValue("@Ip", Ip);
                cmd.Parameters.AddWithValue("@Action", "insert");
                cmd.Parameters.AddWithValue("@PgRequest", PgRequest);
                cmd.Parameters.AddWithValue("@EncRequest", EncRequest);
                cmd.Parameters.AddWithValue("@TId", TId);
                cmd.Parameters.AddWithValue("@IBTrackId", IBTrackId);
                cmd.Parameters.AddWithValue("@ServiceType", ServiceType);
                cmd.Parameters.AddWithValue("@Trip", Trip);
                // add new param 07 sept 2016
                cmd.Parameters.AddWithValue("@PgTotalCharges", TotalPgCharges);
                cmd.Parameters.AddWithValue("@PgTransCharges", TransCharges);
                cmd.Parameters.AddWithValue("@PgChargesType", ChargesType);
                cmd.Parameters.AddWithValue("@PostForm", PostHtml);

                temp = cmd.ExecuteNonQuery();
                con.Close();



            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "InsertPaymentRequestDetails", "insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }

        public static int UpdatePaymentResponseDetails(string orderId, string paymentId, string status, string responseMessage)
        {
            int temp = 0;

            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertPaymentDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TrackId", orderId);
                cmd.Parameters.AddWithValue("@PaymentId", paymentId);
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@ResponseMessage", responseMessage);
                cmd.Parameters.AddWithValue("@ResponseCode", string.Empty);
                cmd.Parameters.AddWithValue("@ErrorText", string.Empty);
                cmd.Parameters.AddWithValue("@PgResponse", responseMessage);
                cmd.Parameters.AddWithValue("@BankRefNo", paymentId);
                cmd.Parameters.AddWithValue("@PgAmount", 0);
                cmd.Parameters.AddWithValue("@PaymentMode", "online");
                cmd.Parameters.AddWithValue("@CardName", string.Empty);
                cmd.Parameters.AddWithValue("@DiscountValue", 0);
                cmd.Parameters.AddWithValue("@MerAamount", 0);
                cmd.Parameters.AddWithValue("@CardType", string.Empty);
                cmd.Parameters.AddWithValue("@IssuingBank", string.Empty);
                cmd.Parameters.AddWithValue("@CardNumber", string.Empty);
                cmd.Parameters.AddWithValue("@UnmappedStatus", "captured");
                cmd.Parameters.AddWithValue("@ApiRequest", string.Empty);
                cmd.Parameters.AddWithValue("@ApiResponse", string.Empty);
                cmd.Parameters.AddWithValue("@ApiStatus", status);
                cmd.Parameters.AddWithValue("@ApiEncryptRequest", string.Empty);

                cmd.Parameters.AddWithValue("@Action", "update");
                temp = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdatePaymentResponseDetails- Update PG Response", "insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }

        public static int UpdateCreditLimit(string agentId, string trackId, string ipAddress)
        {
            int temp = 0;
            
            try
            {
                con.Open();
                cmd = new SqlCommand("AddCreditLimitByPaymentGateway", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AgentID", agentId);
                cmd.Parameters.AddWithValue("@InvoiceNo", trackId);
                cmd.Parameters.AddWithValue("@BookingType", "ESewa Credit");
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                cmd.Parameters.AddWithValue("@ActionType", "PGCREDITLIMIT");
                temp = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdateCreditLimit", "insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }
    }
}
