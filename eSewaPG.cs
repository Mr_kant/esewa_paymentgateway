﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eSewaPayGateway
{
    public class eSewaPG
    {
        public string PaymentGatewayReq(string orderId, string transId, string AgentId, string AgencyName, double TotalAmount, double OrignalAmount, string BillingName, string BillingAddress, string BillingCity, string BillingState, string BillingZip, string BillingTel, string BillingEmail, string ServiceType, string IP)
        {
            string postHtml = string.Empty;
            int isSuccess = 0;

            try
            {
                //string merchantCode = Convert.ToString(ConfigurationManager.AppSettings["eSewaMerchantCode"]);

                //DataTable dtPg_Credentials = eSewaDB.GetPgCredential("eSewa");
                //if (dtPg_Credentials != null && dtPg_Credentials.Rows.Count > 0)
                //{
                //    string ePay_Url = Convert.ToString(dtPg_Credentials.Rows[0]["ProviderUrl"].ToString());
                //    string successUrl = Convert.ToString(dtPg_Credentials.Rows[0]["SuccessUrl"].ToString());
                //    string failedUrl = Convert.ToString(dtPg_Credentials.Rows[0]["FailureUrl"].ToString());

                //postHtml = "<form action='" + ePay_Url + "' method='POST'>"
                //    + "<input value='100' name='tAmt' type='hidden'><input value='90' name='amt' type='hidden'><input value='5' name='txAmt' type='hidden'><input value='2' name='psc' type='hidden'>"
                //    + "<input value='3' name='pdc' type='hidden'><input value='" + merchantCode + "' name='scd' type='hidden'><input value='" + orderId + "' name='pid' type='hidden'>"
                //    + "<input value='" + successUrl + "' type='hidden' name='su'><input value='" + failedUrl + "' type='hidden' name='fu'>"
                //    + "</form>";

                postHtml = PreparePOSTForm(orderId, TotalAmount);
                isSuccess = eSewaDB.InsertPaymentRequestDetails(orderId, transId, "", BillingName, "eSewa", BillingEmail, BillingTel, BillingAddress, TotalAmount, OrignalAmount, AgentId, AgencyName, IP, "", ServiceType, "", "", 0, 0, "", postHtml);
                //}

                if (isSuccess > 0)
                {
                    postHtml = "yes~" + postHtml;
                }
                else
                {
                    postHtml = "no~" + postHtml;
                }
            }
            catch (Exception ex)
            {
                postHtml = "no~" + ex.Message;
                int insert = eSewaDB.InsertExceptionLog("PaymentGateway ", "PaymentGateway", "PaymentGatewayReq", "insert", ex.Message, ex.StackTrace);
                return postHtml;
            }


            return postHtml;
        }

        public string UpdatePaymentResponseDetails(string agentId, string ipAddress, string status, string orderId, string respoAmount, string referenceId, string respoMsg)
        {
            string msg = string.Empty;
            int temp = 0;

            try
            {
                temp = eSewaDB.UpdatePaymentResponseDetails(orderId, referenceId, status, respoMsg);

                if (temp > 0)
                {
                    if (eSewaDB.UpdateCreditLimit(agentId, orderId, ipAddress) > 0)
                    {
                        msg = "yes~" + orderId;
                    }
                    else
                    {
                        msg = "led~" + orderId;
                    }
                }
                else
                {
                    msg = "no~" + orderId;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return msg;
        }

        private string PreparePOSTForm(string orderId, double amount)
        {
            string rutPostHtml = string.Empty;
            try
            {
                DataTable dtPg_Credentials = eSewaDB.GetPgCredential("eSewa");

                if (dtPg_Credentials != null && dtPg_Credentials.Rows.Count > 0)
                {
                    string merchantCode = Convert.ToString(dtPg_Credentials.Rows[0]["MerchantID"].ToString());  //Convert.ToString(ConfigurationManager.AppSettings["eSewaMerchantCode"]);
                    string ePay_Url = Convert.ToString(dtPg_Credentials.Rows[0]["ProviderUrl"].ToString());
                    string successUrl = Convert.ToString(dtPg_Credentials.Rows[0]["SuccessUrl"].ToString());
                    string failedUrl = Convert.ToString(dtPg_Credentials.Rows[0]["FailureUrl"].ToString());

                    rutPostHtml = "<form action='" + ePay_Url + "' method='POST'>"
                            + "<input value='" + amount + "' name='tAmt' type='hidden'><input value='"+ amount + "' name='amt' type='hidden'><input value='0' name='txAmt' type='hidden'><input value='0' name='psc' type='hidden'>"
                            + "<input value='0' name='pdc' type='hidden'><input value='" + merchantCode + "' name='scd' type='hidden'><input value='" + orderId + "' name='pid' type='hidden'>"
                            + "<input value='" + successUrl + "' type='hidden' name='su'><input value='" + failedUrl + "' type='hidden' name='fu'><input value='Submit' type='submit' style='display: none;' id='btnEsewaPg'/>"
                              + "</form><script>document.getElementById('btnEsewaPg').click();</script>";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return rutPostHtml;
        }
    }
}
